# Writing-Public

This repo contains various written works of mine. They're mostly about Linux, security, and other things I might be interested in. 

Please do keep in mind that there's a good chance that my works might include inaccurate information, I'd be happy for you to submit an issue if you feel like the information is untrue, inaccurate, you can submit an issue. If the information you're trying to dispute is a matter of opinion, please avoid making an issue. 

I am not a professional by any means. I acknowledge that many of my opinions may be misguided, or heavily disputed by qualified professionals. If you are seeking information that is trusted by industry leads, pro's, etc, please do your own research. Do not trust me :)


 