# Arch Linux Installation for the Paranoid
## Disclaimer
This guide is largely inspired by another guide made by "Ataraxxia" named `secure-arch`, you can find it here: https://github.com/Ataraxxia/secure-arch/blob/main/00_basic_system_installation.md
Someone also made a video version of the aforementioned guide on YouTube: https://youtu.be/4xeNL7nJLrM (This one slightly alters from the original). 

If you continue to read this guide, please do keep in mind that I am by no means a professional, I am not a security researcher, I am in no way qualified to tell you what is 'secure' and what isn't. This guide is also intended for 'advanced' users. Following this guide will be difficult and take a long time, especially if you're a beginner. 

I am not responsible for any damage, data-loss, etc. that may result from you following along with this guide. **You have been warned**. 

This guide also assumes you have a threat model in mind, if you don't know what that is or haven't thought of one; You can read this article by the Electronic Frontier Foundation: https://ssd.eff.org/module/your-security-plan
## Arch? Why?
 In my opinion: This isn't a great option. Arch Linux has a reputation for being a so-called 'Bleeding Edge' distrobution. It is only as stable as long as you actively work to keep it that way, by reading the news, updating regularly (and manually), etc. Not only that, one of Arch's best features, the AUR or Arch User Repository, contains community published and maintained packages, whether or not you can trust these packages (Package and commit signing is optional) is up to you. If you do chose to use these packages, make sure to thoroughly read the PKGBUILD and understand what it's doing. The reason I picked it, is mostly because of it's popularity and documentation. A lot of people like to use it because it's comfortable to customise, so I decided to write this guide to give people a more 'secure' (See [Disclaimer]) configuration for daily use. 

**If not Arch, then what?**

If your system is a workstation for work...:

I'd like to point in the direction of RHEL8+ based, or repackaged distrobutions. Such as RHEL itself, Rocky Linux, Alma Linux (ABI-only), EuroLinux, etc. These distributions, either RHEL or repackages or RHEL can be used with RHEL documentation, as well as receive frequent security updates, and remain extremely stable. Moreover, they also have good support for OpenSCAP and it's associated security profiles (You can also make your own).

If your system is strictly personal...:

- Qubes OS

	Qubes has become famous for it's highly sophisticated security model. It's been audited and recommended by many security professionals. I personally like it a lot, however it's a lot more demanding and requires beefier hardware than usual linus distrobutions. There's also a learning curve to consider, it's hard to customise, especially if you don't know how to work with Xen or other type 1 hypevisors. 

- Debian/KickSecure	

	Kicksecure is essentially Debian, but packed with hardening techniques that are intended to not interfere with the end user. It has basic and advanced kernel hardening (KSPP, grsec, etc.); disables old technologies and 32-bit emulation (vdso), uses automatically generated AppArmor profiles and system policies for *all* applications, and more. It currently doesn't have an ISO you can download, requiring you to morph an existing Debian installation (with specific requirements) into a Kicksecure installatiom. 
	It is also the parent distribution of Whonix (tm).


But hardening other distrobutions isn't the subject of this article. 

## Security concepts
In this guide I will use various technologies outlined here to hopefully harden the system and make it more secure. Keep in mind that a lot of this will fall in the realm of diminishing returns depending on your use case. 
- Physical security

A lot of this guide will focus on physical security, like encryption and Secure boot setup. However, this goes beyond anything you can do in software.
Never keep your device unattended, if you need to, consider locking it to a wall or a desk. OPSEC. Depending on your use-case, operation security may be a concern. OPSEC is a bit out of scope for this article, but it usually boils down to "Don't be dumb"; "Don't say anything you don't need to". 

- Mandatory access control (MAC)

Mandatory access control is the idea that every action should be defined, allowed or denied. There should be no-defaults. No action, like execution of a program with superuser priveleges should go unchecked or unlogged. In SELinux, this comes in the form of policies, type-enforcement, etc. In this guide; I will use AppArmor as the primary LSM, I think it's pretty user-friendly and SELinux isn't supported on Arch Linux. 

- Principle of least privelege

This applies to all of the aformentioned concepts, but I thought it'd be nice to mention. Nothing should have more permssions or a higher level of privelege than it needs to. All of your programs and software should only be allowed to do things that they *need* to do in order to function. **Please keep this in mind when following along with this guide.** It would be impossible to write a guide for every use-case imaginable, so you'll have to adapt, customise your configuration for your needs. 


## Requirements
- Trust

You should be able to trust the hardware you're installing your OS on.
What counts as 'Trusted' will be left up to the reader. I suggest looking into things like the Intel Management Engine and TXT, as well as possible attack vectors of the system's BIOS/Firmware. Restricting access to physical hardware components (Such as the CMOS/BIOS chip, battery, etc), may also be a way to prevent attacks similar to 'Evil Maiden'.

- Ability to install custom Secure Boot keys

This, naturally, will require your system to be UEFI. This shouldn't be much of an issue, as most machines are UEFI now (post-roughly 2014).  However a lot of UEFI machines (especially laptops) have very locked down BIOS's with hidden options, some of them do not allow wiping or altering factory-set keys. 

- Previous experience installing Arch Linux, or proficiency at the command line

A lot of this guide assumes that the user is experienced enough to understand common linux commands, knows how to navigate the file system, make alterations, etc. If you feel uncomfortable in the command line, this guide isn't really meant for you, although I will try to explain things as best I can. 

PS: I'd recommend reading the official Arch Linux installation guide first, if you have not done so. It includes a lot of useful information for beginners. 

With all of that boilerplate out of the way, let's begin installing Arch Linux! 

## Partitioning, provisioning, encryption. 
The following section includes dangerous commands that will cause unrecoverable data loss. *Please make sure you know what you're doing*, and *please read through this section before actually doing anything.*

Once you've downloaded the official Arch Linux installation CD, verified the image with GPG, flashed it onto *some* bootable media like USB and booted into the installer, you can begin by...

**Wiping the hardrive** 

First of all, we will wipe the drive we're going to install Arch Linux on. This will ensure that no previous information can be recovered after we've installed our OS, and prevent other metadata leaks. Please do keep in mind the following methods do not adhere to any standards, like those published by NIST. (You should not do this in a professional context.)

**WARNING: If you're using SSD or flash media, do not do this.** Use secure erase (Available in most desktop BIOS's) or nvme cleaner. These methods will produce extreme 'Wear and Tear'. 


The following command will zero-fill the drive. 
```
dd if=/dev/zero of=/dev/your_target_drive bs=1M status=progress
```

This command will overwrite your drive with random data, generated via available entropy (RNG). **(Recommended). **

```
dd if=/dev/urandom of=/dev/your_target_drive bs=1M status=progress
```

This process may take a while, depending on the speed and size of your drive. Be patient. 

After that's done, we can move on to...

**Partitioning**

This section is going to be pretty simple. Use an application like `fdisk` or `cfdisk` to create 2 partitions. One as your EFI system partition, and the other as the LUKS partition. The size for the EFI partition should be anywhere between 300MB-1GB. The LUKS partition should fill the rest of the drive. (You can also leave some space if you're planning to dual-boot.)

**LUKS/LVM setup** 

I'm going to use LVM for this configuration, so I can adjust the sizes for my filesystems. If you wish to not do this, you could use BTRFS and subvolumes (As well as it's other features) to adjust filesystem mountpoint options later. 

Format your EFI system partition with FAT32: 
```
mkfs.fat -F 32 /dev/EFI_blockDevice
```

Format your LUKS partition with well... LUKS:

```
cryptsetup luksFormat /dev/crypt_LUKS_block_device
```

Now open that device: (Replace  device_name with anything you want)
```
cryptsetup luksOpen /dev/crypt_LUKS_block_device device_name
```

Create a physical LVM volume 
```
pvcreate /dev/mapper/device_name
```

Create a volume group named "vg0"
```
vgcreate vg0 /dev/mapper/device_name
```

Now create your logical volumes that you will use to mount individual filesystems. At the very minimum, you want a seperate `SWAP`, root, and home volumes. The following is my recommended set of volumes and their individual sizes.:

- `/` or Root. 25-50G
- `/var` 2-5G
- `/var/log` A size of  500MB-1G should be plenty.
- `/var/log/audit` 2-5G. 
- `/var/tmp` In this configuration, I will bind this filesystem to `/tmp`, and then mount it as tmpfs. If you want this volume to reside on disk, also make a separate `/tmp` volume. I'd recommend a size of about 4-8G. 
- `/dev/shm` Optional, this is another temporary filesystem. If you want it to reside on disk, it should be about 4-8G. 
- `/home` This volume should be created last, you can make it as big as you want, or fill up the rest of the remaining volume group. 
- `SWAP` If you have low memory, a size that's equivalent to double available system memory should be sufficient. However, if you have more than 16G of RAM, anything higher than 16G is unnecessary. 

To create volumes, run this command. 
```
lvcreate -L Size_In_GB vg0 -n volume_name
```

Replace the `Size_In_Gb` with how big you want that volume to be; Replace `volume_name` with anything, although I name it after it's mountpoint. E.g. home, var, vartmp, etc. 
This command also specified the volume group `vg0`, that we created earlier. 

Example, create a volume that's 4G in size, and has the name `var`:

```
lvcreate -L 4G vg0 -n var
```

After you're done creating all of your volumes except for `/home`, you do so now by filling up the rest of the volume group:

```
lvcreate -l 100%FREE vg0 -n home
```
These volumes cannot be used yet, as they do not contain a filesystem. So let's move on to...

**Formatting**

This is going to be a matter of preference, personally, I like to use XFS for everything except `/`, `/home`, `/var/log`, `/var/log/audit` which will be formatted with ext4. 

Remember to replace `volume_name` with the volume you wish to format. 

To format using ext4 run: `mkfs.ext4 /dev/mapper/vg0-volume_name`

To format using XFS run: `mkfs.xfs /dev/mapper/vg0-volume_name`

Now we can finally move on to...
## System Bootstrap

**Mounting filesystems**

Okay, now that you have formatted all of your volumes, you're gonna have to mount them. First, mount the root volume:

```
mount /dev/mapper/vg0-root /mnt # Replace 'root' with whatever you named the root volume.
```

Then create directories in `/mnt` that correspond to your created volumes. Remember to mount the parent filesystem directory first. E.g.:
`mkdir /mnt/var && mount /dev/mapper/vg0-var /mnt/var`, before creating and mounting `/mnt/var/log`, so on and so forth. 

After you've created all of the directories, as well as mounted all the volumes, you're ready to install the Arch Linux base system. Remember to connect to the internet if you haven't already. Use `dhcpcd` for ethernet (If this hasn't been done automatically) and `iwctl` for wifi adapters. 

**Installing the basesystem**


 I recommend using the linux-hardened kernel variant, installing a bunch of utilities like a text editor and sudo; Dracut, lvm2, sbsigntools for making the system bootable and the secureboot setup later on, etc. You could also look into installing the -headers package for your kernel, as well as the firmware (linux-firmware) if you have any hardware that depends on proprietary kernel blobs. If you need manuals, install man-db as well. 
I'd also recommend installing your CPU's microcode. For Intel, replace `YOUR_UCODE_PACKAGE` with `intel-ucode`, for AMD,  `amd-ucode`.
```
pacstrap /mnt base  linux-hardened YOUR_UCODE_PACKAGE sudo neovim lvm2 dracut sbsigntools networkmanager efibootmgr binutils dhcpcd # Adjust as necessasry for your configuration.
```
## System Configuration

**Filesystem TAB configuration**

First you can begin by generating the initial file (Optional)

```
genfstab -U /mnt >> /mnt/etc/fstab
```

If you choose to configure this file manually without using `genfstab`, you should use UUID's, since they are more reliable. You can look at your volume's associated UUID's by using `blkid` or `lsblk -f`. 

Now we'll have to edit this file to set mount options for all of our volumes. Here's my recommended configuration.:
```
# Replace /dev/mapper/vg0-volume_name with that volume's UUID. 
/dev/mapper/vg0-root     /                       ext4    defaults,x-systemd.device-timeout=0,relatime,errors=remount-ro 1 1
/dev/EFI_blockDevice         /boot/efi               vfat    umask=0022,fmask=0022,codepage=437,iocharset=ascii,shortname=mixed,utf8,errors=remount-ro 0 2
/dev/mapper/vg0-home     /home                   ext4    nodev,nosuid,rw,relatime,x-systemd.device-timeout=0,errors=remount-ro 1 2
/dev/mapper/vg0-var      /var                    ext4     defaults,nodev,nosuid,x-systemd.device-timeout=0 0 0
/dev/mapper/vg0-var_log  /var/log                ext4    defaults,nodev,noexec,nosuid,x-systemd.device-timeout=0 1 2
/dev/mapper/vg0-var_log_audit /var/log/audit          ext4    defaults,nodev,noexec,nosuid,x-systemd.device-timeout=0 1 2
/dev/mapper/vg0-var_tmp  /var/tmp                xfs     defaults,nodev,noexec,nosuid,x-systemd.device-timeout=0 0 0
/dev/mapper/vg0-swap     none                    swap    defaults,x-systemd.device-timeout=0 0 0
# PS: Adding 'noexec' to the mount options will prevent legitimate scripts from running in that filesystem. 
# I Add this option since I never need to use scripts that do this. 

# Optional. Comment the following 2 lines if you have not created a seperate /tmp and /dev/shm volume. 
/dev/mapper/vg0-dev_shm /dev/shm auto defaults,x-systemd.device-timeout=0,nodev,noexec,nosuid 0 0
/dev/mapper/vg0-tmp      /tmp                    xfs     defaults,nodev,noexec,nosuid,x-systemd.device-timeout=0 0 0

# tmpfs for /tmp, bind /var/tmp to /tmp. Uncomment if you haven't created a seperate `/tmp` and `/var/tmp` volume. 
#tmpfs /tmp tmpfs rw,nosuid,noatime,nodev,size=4G,mode=1777 0 0
#/tmp	/var/tmp	tmpfs bind,nosuid,noatime,nodev,nodirtime,noexec	0 0
```

**System Configuration (chroot)**

Begin by chrooting into the installation: 

```
arch-chroot /mnt
```
 
First things first, lock the root account so that nobody can log into it. This account should NEVER be used by any means, all of your administrative tasks should be accomplished with `sudo`.

```
passwd -l root
```

**Account Creation**

Now you can create your user accounts, I would recommend creating 2 of them, one as an "Administrator" account for handling package updates, installation, etc. And the other for daily desktop usage. 
Create a user in the wheel group for administration: 
```
useradd -m -G wheel -s /bin/bash admin_user # Replace admin_user with what you want your admin user to be called. 
```

Then add a desktop user:
```
useradd -m -G video -s /bin/bash desktop_user # Replace desktop_user with what you want your desktop user to be called.
```

Remember to set strong individual passwords for both of them using `passwd` followed by their username.

Edit your `/etc/sudoers` file, so your administrator account can use sudo by uncommenting the line portaining to the wheel group. DO NOT allow passwordless sudo.  Keep in mind that this should be temporary, you should configure your administrator account to only execute commands required for daily use. But this will work for now. 

PS: If vim (or neovim) complains about the file being read-only, add a `!` to force the edit. so if you want to write & quit, it would be `:wq!` You could also set neovim or whatever text editor you use to `visudo`. 


**Timezones**

Set your timezone (TZ).

EXAMPLE: If you're from Berlin, the filepath would be ../Europe/Berlin

```
ln -sf /usr/share/zoneinfo/<Region>/<city> /etc/localtime
```

**Locale configuration**

Edit the /etc/locale.gen file and uncomment the locales you would like to generate. For English US, uncomment en_US.UTF-8 .
Then generate them:

```
locale-gen
```

Optionally you can also configure your console keyboard layout, the one which is used in the TTY for example. If you have a US layout, you can skip this part.  Simply edit the /etc/vconsole.conf file.

**Networking** 

 We can now enable our networking services.

```
systemctl enable dhcpcd # Optional
systemctl enable NetworkManager
```
Now we're going to setup the firewall. I'm going to use `ufw` to do this. 
Block all incoming requests, except on ports `80` and `443` (HTTP/HTTPS).:

```
ufw allow 80/tcp  
ufw allow 443/tcp  
ufw default deny incoming  
ufw default allow outgoing
ufw enable
```

That is a pretty lenient configuration, you could also look into denying all OUTGOING and INCOMING requests, 
and whitelisting applications one by one.

**Allow Secure Shell (SSH)**

Optionally, you could also allow SSH connections on port 22. 
We limit port 22, so that it's rate limited.
```
 ufw limit 22/tcp 
```

*Also, if you're going to use SSH, you should also configure fail2ban.*

 Install: `paman -S fail2ban`

Then create the `/etc/fail2ban/jail.local` file and paste in the following: 
```
[DEFAULT]
 ignoreip = 127.0.0.1/8 ::1 # Allow localhost to connect with no restrictions
 bantime = 3600
 findtime = 600
 maxretry = 5
 
[sshd]
 enabled = true
```
Enable the fail2ban service:

```
systemctl enable fail2ban
```


## Unified Kernel Image Setup
In this configuration, I will not be using a bootloader. Instead, I will create a UKI image and boot directly from UEFI. 


We're going to create 2 scripts, one that installs the UKI image to `/boot/efi/EFI/Linux/`, and one which deletes the image.



Create the `/usr/local/bin/uki-install` file and add the following:  (Stolen from Ataraxxia)

```
#!/usr/bin/env bash

	mkdir -p /boot/efi/EFI/Linux

	while read -r line; do
		if [[ "$line" == 'usr/lib/modules/'+([^/])'/pkgbase' ]]; then
			kver="${line#'usr/lib/modules/'}"
			kver="${kver%'/pkgbase'}"
	
			dracut --force --uefi --kver "$kver" /boot/efi/EFI/Linux/arch-linux.efi
		fi
	done
```

Create the `/usr/local/bin/uki-remove` file and add the following: 

```
#!/usr/bin/env bash
 	rm -f /boot/efi/EFI/Linux/arch-linux.efi
```

Make both of them executable:

```
chmod +x /usr/local/bin/uki-*
```
**Pacman Hooks**

Configure pacman hooks, so that when you upgrade your kernel, this will allow pacman automatically remove and create the UKI image. 

Create the hooks directory:

```
mkdir /etc/pacman.d/hooks
```


Create the `/etc/pacman.d/hooks/90-dracut-install.hook` file and add the following (stolen from Ataraxxia):

```
[Trigger]
	Type = Path
	Operation = Install
	Operation = Upgrade
	Target = usr/lib/modules/*/pkgbase
	
	[Action]
	Description = Updating linux EFI image
	When = PostTransaction
	Exec = /usr/local/bin/uki-install
	Depends = dracut
	NeedsTargets
```

Create the `/etc/pacman.d/hooks/60-dracut-remove.hook` file and add the following (stolen from Ataraxxia):

Add the following: 
```
[Trigger]
	Type = Path
	Operation = Remove
	Target = usr/lib/modules/*/pkgbase
	
	[Action]
	Description = Removing linux EFI image
	When = PreTransaction
	Exec = /usr/local/bin/uki-remove
	NeedsTargets
```

**Initial UKI creation**

We must configure dracut to pass the neccessary kernel command line parameters for `cryptsetup` and lvm to work properly. 

Edit said file and add the following: 
```
kernel_cmdline="rd.luks.uuid=luks-YOUR_UUID rd.lvm.lv=vg0/root root=/dev/mapper/vg0-root rootfstype=btrfs rootflags=rw,relatime"
```
Replace "YOUR_UUID" with the UUID of your LUKS partition, once again, you can find that UUID by running `blkid` or `lsblk -f`. 
Also remember to replace the "rootfstype" flag with whatever you formatted your root volume with. 

We're going to set some flags for dracut to compress the UKI image with `zstd`. Create `/etc/dracut.conf.d/flags.conf` and add the following:
```
compress="zstd"
hostonly="no"
```

At this point, you should re-install your kernel, so that the scripts and pacman hooks we created  generate  the UKI image using dracut.:

```
pacman -S linux-hardened
```
**This next step could be platform dependent...**

Ataraxxia - in their guide, mentioned that some older platforms can ignore `efibootmanager` entries all together and look for `EFI\BOOT\bootx64.efi` instead,  in that case you may generate your UKI directly to that directory and under that name. It's very important that the name is also bootx64.efi.

**Creating UEFI boot entries**

Use `efibootmgr` to create a UEFI boot entry pointing to your UKI image:

```
efibootmgr --create --disk /dev/EFI_blockDevice --part 1 --label "Arch Linux" --loader 'EFI\Linux\arch-linux.efi' --unicode
```

Run `efibootmgr` again to check if the boot entry is actually there. 

*At this point, this is enough to create a bootable system. However, we are still not done.*

## Secure Boot Setup
Assuming you're in setup mode, you can now create your keys, enroll them, sign your UKI image. 

First, install `sbctl` or Secure-Boot control. 
`pacman -S sbctl`

Create the keys:
`sbctl create-keys`

Sign your UKI image: 
`sbctl sign -s /boot/efi/EFI/Linux/arch-linux.efi`

PS: Do not double sign your binaries! That would only be useful if you sign files part of Microsoft's bootchain. 

To configure dracut to use uefi_secureboot, create the `/etc/dracut.conf.d/secureboot.conf` file and add the following:

```
uefi_secureboot_cert="/usr/share/secureboot/keys/db/db.pem"
uefi_secureboot_key="/usr/share/secureboot/keys/db/db.key"
```

Create a pacman hook to automatically sign a new the newly created UKI image by creating the `/etc/pacman.d/hooks/zz-sbctl.hook` file and add the following: 

```
[Trigger]
	Type = Path
	Operation = Install
	Operation = Upgrade
	Operation = Remove
	Target = boot/*
	Target = efi/*
	Target = usr/lib/modules/*/vmlinuz
	Target = usr/lib/initcpio/*
	Target = usr/lib/**/efi/*.efi*

	[Action]
	Description = Signing EFI binaries...
	When = PostTransaction
	Exec = /usr/bin/sbctl sign /boot/efi/EFI/Linux/arch-linux.efi
```
**Key enrollment**

PS:`sbctl` does NOT sign your images using Microsoft's own keys. It does not have access to Microsofts private keys, only the ones you created using `sbctl create-keys`.


To enroll your custom keys into the UEFI:

`sbctl enroll-keys`

If the command fails, that means your platform **requires** you to append your keys with Microsoft's CA, or to supply a tpm2 eventLog, if present. However, it's much easier to just run:
`sbctl enroll-keys --microsoft`

After you've completed your install, you should reboot to your UEFI and enable secureboot in custom mode. If possible, you should disable PXE/LAN  and USB booting. 

## AppArmor Setup
AppArmor allows you to sandbox your applications by creating 'profiles' for them to only allow them to do things they're supposed to and denying things they're not supposed to do. 

**Installation**

First, append this line to your kernel command line parameters to the file we created earlier.
Paste this line in `/etc/dracut.conf.d/cmdline.conf` :

```
lsm=landlock,lockdown,yama,integrity,apparmor,bpf
```

Then enable the AppArmor service:

```
systemctl enable apparmor.service
```

Re-install your kernel so dracut appends those new kernel parameters in your UKI image:
`pacman -S linux-hardened` 

**Verification** 

After finishing your installation and booting into it, you  can verify apparmor is loaded and active by running `sudo aa-status` with your administrator account.

**How do I use it?**

The concept is simple. It has several modes. `enforcing` , `complain` and `unconfined`.

Enforcing means that the applications profile will allow things it's supposed to do and deny things it's not supposed to do, as per the defined profile. 
Complain mode means that the applications profile will allow everything specified and **not** deny actions that are not defined in the profile but will log them. 
Unconfined mode essentially that no actions are made. Everything is allowed and denied actions are not logged. 

In order to create a profile for a specific application, in this example let's say `ani-cli`, 
you want to run `sudo aa-genprof /usr/bin/ani-cli` in your administrator account. When you run this command, you want to start using your application. Make sure you do everything from searching to accessing specifc files if need-be. As you do this, you want to go back to the terminal you ran the` aa-genprof` command on and press S to scan for log changes, then choose what you want it to do (Keep in mind some stuff might be critical for functionality) and if it needs access to a certain file, I would just choose (I)nherit. When you're done, you can save the changes and Finish creating the profile, this will apply the profile in `enforce` mode. 

You could also create your profile manually in `/etc/apparmor.d/`, I've found a repository with a lot of these AppArmor profiles, keep in mind that it can only be installed through the AUR with krathalan-apparmor-profile-git.  You could also copy new and updates profies from  https://github.com/krathalan/apparmor-profiles, however this is a bit tedious. I'd still recommend creating profiles for all of your applications manually.  

If your profile breaks the application in some way, you can disable it with `aa-disable`. Or to disable ALL profiles (and unload apparmor modules) run `aa-teardown`. 

## Additional hardening

**Less is more**

This is probably the easiest thing you could implement. Don't install meaningless crap you're never gonna use. Only install what is necessasry, keep applications to a minimum. Delete system caches, history, file history frequently, uninstall anything you might not be using anymore. The less applications you use, the smaller your attack surface is. (In theory anyway)

In this section I will cover the following.:

- Boot-time security

Additional kernel commandline parameters to enhance security.

- Runtime security (sysctl's)

Use sysctl settings to further enhance security, and minimise the network stack attack surface. 

- Miscellaneous tools, programs and configurations

USBGuard, ClamAV, browser extensions, 2FA, UBI keys, so on and so forth. 

Let's start with the more simple stuff. 

**Recommended kernel parameters (KSPP)**

These options were taken from the KSPP wiki at kernsec.org, I'd recommend reading it. http://www.kernsec.org/wiki/index.php/Kernel_Self_Protection_Project/Recommended_Settings

Append these options to your `/etc/dracut.conf.d/cmdline.conf` file: 

```
hardened_usercopy=1 init_on_alloc=1 init_on_free=1 randomize_kstack_offset=on page_alloc.shuffle=1 slab_nomerge pti=on iommu.passthrough=0 iommu.strict=1 YOURPLATFORM_iommu=1 mitigations=auto
```
Replace 'YOURPLATFORM' with either `intel` or `amd` respectively. 

You may also add these options, although they will slow down your system considerably. 

```
mitigations=auto,nosmt slab_debug=ZF
```
**Recommended sysctl's**

Create the `/etc/sysctl.d/00-kspp-hardening.conf` file, add the following: (Stolen from the KSPP wiki, recommended settings)
```
kernel.kptr_restrict = 2
kernel.dmesg_restrict = 1
kernel.perf_event_paranoid = 3

# Turn off kexec, even if it's built in.
kernel.kexec_load_disabled = 1

kernel.randomize_va_space = 2

# This disables all of ptrace. If you need ptrace to work, set the value to '1'. 
kernel.yama.ptrace_scope = 3

# Dangerous, may break apparmor LSM, as well as other applications like KVM that require user namespaces, please evaluate your workload. If unsure, do not set this option.
user.max_user_namespaces = 0

dev.tty.ldisc_autoload = 0
dev.tty.legacy_tiocsti = 0

kernel.unprivileged_bpf_disabled = 1

net.core.bpf_jit_harden = 2
vm.unprivileged_userfaultfd = 0

# Filesystem related sysctl's. 
fs.protected_symlinks = 1
fs.protected_hardlinks = 1
fs.protected_fifos = 2
fs.protected_regular = 2
fs.suid_dumpable = 0

# Harden network stack (This 'collection' of options was suggested by athraxxia.)

# Do not act as a router
net.ipv4.ip_forward = 0
net.ipv6.conf.all.forwarding = 0

# SYN flood protection
net.ipv4.tcp_syncookies = 1

# Disable ICMP redirect
net.ipv4.conf.all.accept_redirects = 0
net.ipv4.conf.default.accept_redirects = 0
net.ipv4.conf.all.secure_redirects = 0
net.ipv4.conf.default.secure_redirects = 0
net.ipv6.conf.all.accept_redirects = 0
net.ipv6.conf.default.accept_redirects = 0

# Do not send ICMP redirects
net.ipv4.conf.all.send_redirects = 0
net.ipv4.conf.default.send_redirects = 0
```
Apply these sysctl's  by running: `sudo sysctl --system` as your administrative user.

**Module signing, kernel lockdown**
By following the https://wiki.archlinux.org/title/Signed_kernel_modules article, you should be able to setup kernel module signing. (Out of tree) 

Since we used the `linux-hardened` kernel, you should just be able to use the provded helper script in the AUR. 
Verify that there are no unsigned kernel modules loaded on the system by running: 

```
for mod in $(lsmod | tail -n +2 | cut -d' ' -f1); do modinfo ${mod} | grep -q "signature" || echo "no signature for module: ${mod}" ; done
```

After that's done, you can enable kernel lockdown mode by appending the following kernel cmdline parameters.:

```
module.sig_enforce=1 lockdown=confidentiality
```


**Making use of flatpak**

Flatpak, in terms of security, has a lot of issues. The way security is handled misleads users to think of their applications as being 'Sandboxed', when in reality they have R/W access to your system files, or home directory. 
There are 2 articles that are a bit harsh on flatpak, but I feel like are worth checking out. https://flatkill.org/, and https://flatkill.org/2020/. 

We can take matters into our own hands, by modifying flatpak permissions to never have access to the home directory, and only have access to folders that we designate. Unfortunately, if there are any security vulnerabilities in flatpak or it's runtimes, we can't really do much until the maintainers issue a fix and update. 

For this, I like to use an application called flatseal. Keep in mind it does require R/O access to the `/var/lib/flatpak` and `/var/lib/flatpak/app` directories. 

PS: Never add remotes or install software system-wide when using flatpak. Always use the `--user` flag, to only install the application for your desktop user. I would even go as far as to change ownership of `/var/lib/flatpak` and `/var/lib/flatpak/app` directories to root with `chown root:root`, but this breaks a lot of apps like the aforementioned flatseal. 

**Securely browsing the web**

I'd strongly suggest using a broswer with sensible defaults like Librewolf (https://librewolf.net/), it's available in the AUR as a binary. (`librewolf-bin`). You may also want to install several extensions to further enhance your privacy, although, remember that you should never install untrusted or closed-source extensions and keep them to a very strict minimum; 

- NoScript, don't allow websites to execute JavaScript and other functions without permission. 
- LocalCDN, emulates Content Delivery Networks to improve your online privacy.
- CanvasBlocker, allows for better protection against fingerprinting, from alright blocking to faking certain forms of fingerprinting.
- ClearURL's, will automatically remove tracking elements from URLs.

If you really wanna use a chromium based browser, I suggest `ungoogled-chromium`.

**USB device protection**

If you're even more concerned about physical security, you can setup `usbguard` (https://usbguard.github.io/) to block external and unrecognized USB devices as well as  `usbkill` which actively shuts down your computer when an unrecognised USB device is connected. 
Wiki page: https://wiki.archlinux.org/title/USBGuard

**ClamAV antivirus**

This is to be used to scan files for malware. I'd recommend doing this if you're going to install any and all software  from a third party. (Like a github script or executable file you downloaded from a website). You could also setup OnAccessScan to automatically scan files when they've been renamed, moved or created (AND OR) modified in some way.
Wiki page: https://wiki.archlinux.org/title/ClamAV

**Auditing**

`lynis` is a tool that will analyze your system and give you tips on how to harden your system even further. It's main goals are:
- Automated security auditing
- Compliance testing (e.g. ISO27001, PCI-DSS, HIPAA)
- Vulnerability detection

You can also install `rkhunter`, which is designed to audit your system for possible rootkits. 

You can also look into `openscap` and OpenSCAP's profiles for standards compliance, security profiles, etc. However the compatibility with arch may not be as good as it is on RHEL-based systems. 

**Universal 2nd Factor (U2F)** 

Using U2F, you can use a USB security token (Like a Yubikey for example) to handle various authentication related tasks on your system. Like logging in with sudo, GDM, making use of the PAM module `pam-u2f`, OpenSSH, "Data-at-rest encryption" using LUKS. 
Wiki page: https://wiki.archlinux.org/title/Universal_2nd_Factor

**Advanced Intrusion Detection Environment (AIDE)**

AIDE Is a host-based intrusion detection system (HIDS) for checking the integrity of files. This can allow you to automatically detect if a file has been tampered with. AIDE **DOES NOT** check for rootkits or look through logfiles for suspicious activity (Like OSSEC for example). It can only check the integrity of files. 
Wiki page: https://wiki.archlinux.org/title/AIDE

# Closing Thoughts
Thank you for reading this entire thing. There are still a couple of issues and readability things I'd like to sort out in the future, but I appreciate you for sticking around this long. A couple things to keep in mind, which mostly boil down to 'Don't be dumb': 

- Try to compartmentalize your workflow, separate your digital, personal, work, education lives from one another. 
- Try to sandbox as many things as possible, like through AppArmor.
- Try to configure the minimum required permissions for everything, if a program doesn't need to do X, it shouldn't have the capability to do so. 

Stay safe out there. Stay sane. LGTM!

Thank you. 


